const getData = (() => {
    fetch('./data.json').then((response) => {
        return response.json();
    }).then((data) => {
        // console.log(data);
        toggle(data);
    }).catch((err) => {
        console.log(err);
    })
})

function toggle(data) {
    let toggleSwitch = document.getElementById("toggle-switch");
    if (toggleSwitch.checked == true) {
        document.getElementById("price-1").innerText = `${data.monthly.basic}`;
        document.getElementById("price-2").innerText = `${data.monthly.professional}`;
        document.getElementById("price-3").innerText = `${data.monthly.master}`;
    } else {
        document.getElementById("price-1").innerText = `${data.annually.basic}`;
        document.getElementById("price-2").innerText = `${data.annually.professional}`;
        document.getElementById("price-3").innerText = `${data.annually.master}`;
    }
}